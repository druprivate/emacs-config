;; init.el --- Emacs configuration

;; INSTALL PACKAGES
;; -----------------------------------------------------------------------------

;; https://elpy.readthedocs.io/en/latest/introduction.html
;; https://www.emacswiki.org/emacs/MELPA
;; (require 'package)
;; (add-to-list 'package-archives
;;              '("melpa-stable" . "https://stable.melpa.org/packages/"))
;;              '("melpa" . "https://melpa.org/packages/"))

;; pb when installing package : see
;; https://stackoverflow.com/questions/24833964/package-listed-in-melpa-but-not-found-in-package-install
;; then M-x package-list-packages
(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
    )

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(avy
    better-defaults
    ein
    elpy
    ;; ensime
    ess ;; for R
    flycheck
    ;;    helm
    ;;    highlight-indent-guides
    magit
    material-theme
    markdown-mode
    pungi
    pyvenv
    py-autopep8
    smooth-scrolling
    sql-indent
    sqlup-mode
    sr-speedbar
    vimish-fold
    yafolding
    yasnippet))

(mapc #'(lambda (package)
          (unless (package-installed-p package)
            (package-install package)))
      myPackages)

;; tell emacs where is your personal elisp lib dir
;; http://ergoemacs.org/emacs/emacs_installing_packages.html
(add-to-list 'load-path "~/.emacs.d/elisp/")

;; BASIC CUSTOMIZATION
;; -----------------------------------------------------------------------------

(setq inhibit-startup-message t) ;; hide the startup message
(load-theme 'material t) ;; load material theme
(global-linum-mode t) ;; enable line numbers globally
(set-language-environment "UTF-8")

;; font size
;; The value is in 1/10pt, so 100 will give you 10pt, etc.
(set-face-attribute 'default nil :height 200)

;; http://blog.nguyenvq.com/blog/2013/07/26/automatically-capitalize-or-uppercase-or-expand-keywords-in-emacs-using-abbrev-mode/
;; stop asking whether to save newly added abbrev when quitting emacs
(setq save-abbrevs nil)
;; turn on abbrev mode globally
(setq-default abbrev-mode t)

;; remove tabs
;; https://stackoverflow.com/questions/1012855/how-to-set-emacs-tabs-to-spaces-in-every-new-file
(setq-default indent-tabs-mode nil)

;; remove trailing whitespace
;; https://www.emacswiki.org/emacs/DeletingWhitespace
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; replace tabs with whitespace
;; https://www.emacswiki.org/emacs/UntabifyUponSave
(defvar untabify-this-buffer)
(defun untabify-all ()
  "Untabify the current buffer, unless `untabify-this-buffer' is nil."
  (and untabify-this-buffer (untabify (point-min) (point-max))))
(define-minor-mode untabify-mode
  "Untabify buffer on save." nil " untab" nil
  (make-variable-buffer-local 'untabify-this-buffer)
  (setq untabify-this-buffer (not (derived-mode-p 'makefile-mode)))
  (add-hook 'before-save-hook #'untabify-all))
(add-hook 'prog-mode-hook 'untabify-mode)

;; backup files
;; https://stackoverflow.com/questions/2680389/how-to-remove-all-files-ending-with-made-by-emacs/25692389
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; how many of the newest versions to keep
      kept-old-versions 5    ; and how many of the old
      )

;; ibuffer as default
;; http://ergoemacs.org/emacs/emacs_buffer_management.html
(defalias 'list-buffers 'ibuffer) ; make ibuffer default

;; support mouse in terminal mode
(when (eq window-system nil)
  (xterm-mouse-mode t))

;; PYTHON CONFIGURATION
;; -----------------------------------------------------------------------------

(package-initialize)
(elpy-enable)
;;(elpy-use-ipython)

;; use flycheck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)
(setq py-autopep8-options '("--max-line-length=100"))

;; http://jblevins.org/projects/markdown-mode/
(require 'markdown-mode)

;; http://stackoverflow.com/questions/7997590/how-to-change-the-default-split-screen-direction
(setq split-width-threshold nil)

;; http://tiborsimko.org/emacs-epydoc-snippets.html
;; http://ergoemacs.org/emacs/yasnippet_templates_howto.html
(require 'yasnippet)
;; (yas/initialize)
;; (yas/load-directory "~/.emacs.d/snippets")
(add-to-list 'load-path "~/.emacs.d/snippets")


;; http://tech.novapost.fr/configurer-emacs-comme-un-developpeur-python.html
(setq-default indent-tabs-mode nil)  ; use only spaces and no tabs
(setq default-tab-width 4)

;; To load python templates
;; http://blog.ishans.info/2012/06/10/adding-a-template-for-new-python-files-in-emacs/
(add-hook 'find-file-hooks 'maybe-load-template)
(defun maybe-load-template ()
  (interactive)
  (when (and
         (string-match "\\.py$" (buffer-file-name))
         (eq 1 (point-max)))
    (insert-file "~/.emacs.d/templates/template.py")))

3(require 'magit)
(global-set-key (kbd "C-x g") 'magit-status)


;; https://www.reddit.com/r/emacs/comments/3kqt6e/2_easy_little_known_steps_to_speed_up_emacs_start/
;; https://www.reddit.com/r/emacs/comments/24hhgm/why_is_emacs_so_slow_on_startup_compared_to_vim/
(emacs-init-time)
(length load-history)


;; http://stackoverflow.com/questions/9688748/emacs-comment-uncomment-current-line
(defun toggle-comment-on-line ()
  "comment or uncomment current line"
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

;; define keys : http://ergoemacs.org/emacs/keyboard_shortcuts.html
(global-set-key (kbd "M-c") 'comment-region)
(global-set-key (kbd "M-v") 'uncomment-region)
(global-set-key (kbd "<f5>") 'revert-buffer)
(global-set-key (kbd "C-l") 'toggle-comment-on-line)

;; SCIPY CUSTOMIZATION
;; -----------------------------------------------------------------------------
;; http://www.scipy-lectures.org/advanced/debugging/

(defun pyflakes-thisfile () (interactive)
       (compile (format "pyflakes %s" (buffer-file-name)))
       )

(define-minor-mode pyflakes-mode
  "Toggle pyflakes mode.
    With no argument, this command toggles the mode.
    Non-null prefix argument turns on the mode.
    Null prefix argument turns off the mode."
  ;; The initial value.
  nil
  ;; The indicator for the mode line.
  " Pyflakes"
  ;; The minor mode bindings.
  '( ([f5] . pyflakes-thisfile) )
  )

(add-hook 'python-mode-hook (lambda () (pyflakes-mode t)))

(when (load "flymake" t)
  (defun flymake-pyflakes-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list "pyflakes" (list local-file))))

  (add-to-list 'flymake-allowed-file-name-masks
               '("\\.py\\'" flymake-pyflakes-init)))

(add-hook 'find-file-hook 'flymake-find-file-hook)


;; R
;; -----------------------------------------------------------------------------

(require 'ess)

;; RST
;; -----------------------------------------------------------------------------

;; http://docutils.sourceforge.net/tools/editors/emacs/rst.el
(require 'rst)

;; init.el ends here

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (py-autopep8 markdown-mode material-theme magit flycheck elpy pungi better-defaults))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; rst template
(add-hook 'find-file-hooks 'maybe-load-template-rst)
(defun maybe-load-template-rst ()
  (interactive)
  (when (and
         (string-match "\\.rst$" (buffer-file-name))
         (eq 1 (point-max)))
    (insert-file "~/.emacs.d/templates/template.rst")))

;; SQL
;; -----------------------------------------------------------------------------
;; https://github.com/purcell/emacs.d/blob/master/lisp/init-sql.el
;; sql-indent mode
(eval-after-load "sql"
  '(load-library "sql-indent"))
;; https://github.com/alex-hhh/emacs-sql-indent
;; To enable syntax-based indentation for every SQL buffer, you can add sqlind-minor-mode to sql-mode-hook
(add-hook 'sql-mode-hook 'sqlind-minor-mode)

;; https://github.com/kmmbvnr/emacs-config/blob/master/init.el
;; (defvar config-dir (file-name-directory load-file-name))
;; (load (concat config-dir "elisp/plsql.el"))
;; (setq auto-mode-alist
;; (append
;; '(("\\.\\(p\\(?:k[bg]\\|ls\\)\\|sql\\)\\'" . plsql-mode))
;; auto-mode-alist))

;; https://emacs.stackexchange.com/questions/13214/automatically-formatting-sql-code
;; https://github.com/Trevoke/sqlup-mode.el/blob/master/README.md
(require 'sqlup-mode)
;; Capitalize keywords in SQL mode
(add-hook 'sql-mode-hook 'sqlup-mode)
;; Capitalize keywords in an interactive session (e.g. psql)
(add-hook 'sql-interactive-mode-hook 'sqlup-mode)
;; Set a global keyword to use sqlup on a region
(global-set-key (kbd "C-c u") 'sqlup-capitalize-keywords-in-buffer)

;; ignore certain keywords by adding them to the list sqlup-blacklist.
(add-to-list 'sqlup-blacklist "timestamp")
(add-to-list 'sqlup-blacklist "key")

;; capitalize keywords
;; https://www.emacswiki.org/emacs/SqlUpcase
;; https://www.emacswiki.org/emacs/sql-upcase.el
(when (require 'sql-upcase nil :noerror)
  (add-hook 'sql-mode-hook 'sql-upcase-mode)
  (add-hook 'sql-interactive-mode-hook 'sql-upcase-mode))

;; auto-format sql buffer at opening
(add-hook 'find-file-hooks 'format-sql-buffer-at-opening)
(defun format-sql-buffer-at-opening ()
  "Select all text, and cut/paste it"
  (interactive)
  (when
      (string-match "\\.sql$" (buffer-file-name))
    (mark-whole-buffer)
    (kill-region (point-min) (point-max))
    (yank)
    (deactivate-mark)
    (beginning-of-buffer)
    (message "Buffer reformatted.")
    )
  )
(global-set-key (kbd "<f9>") 'format-sql-buffer-at-opening)


;; BASH
;; -----------------------------------------------------------------------------
;; bash template
(add-hook 'find-file-hooks 'maybe-load-template-bash)
(defun maybe-load-template-bash ()
  (interactive)
  (when (and
         (string-match "\\.bash$" (buffer-file-name))
         (eq 1 (point-max)))
    (insert-file "~/.emacs.d/templates/template.bash")))

;; code folding
;; -----------------------------------------------------------------------------
;; https://www.reddit.com/r/emacs/comments/746cd0/which_code_folding_package_do_you_use/

(defun toggle-selective-display (column)
  (interactive "P")
  (set-selective-display
   (or column
       (unless selective-display
         (1+ (current-column))))))

(defun toggle-hiding (column)
  (interactive "P")
  (if hs-minor-mode
      (if (condition-case nil
              (hs-toggle-hiding)
            (error t))
          (hs-show-all))
    (toggle-selective-display column)))

(load-library "hideshow")
(global-set-key (kbd "C-+") 'toggle-hiding)
(global-set-key (kbd "C-\\") 'toggle-selective-display)

(add-hook 'python-mode-hook       'hs-minor-mode)

;; (use-package hideshow
;;   :hook ((prog-mode . hs-minor-mode)))

;; (defun toggle-fold ()
;;   (interactive)
;;   (save-excursion
;;     (end-of-line)
;;     (hs-toggle-hiding)))

;; (vimish-fold-global-mode 1)

;; https://stackoverflow.com/questions/1085170/how-to-achieve-code-folding-effects-in-emacs
(global-set-key (kbd "<f6>") 'set-selective-display-dlw)

(defun set-selective-display-dlw (&optional level)
  "Fold text indented same of more than the cursor.
If level is set, set the indent level to LEVEL.
If 'selective-display' is already set to LEVEL, clicking
F6 again will unset 'selective-display' by setting it to 0."
  (interactive "P")
  (if (eq selective-display (1+ (current-column)))
      (set-selective-display 0)
    (set-selective-display (or level (1+ (current-column))))))

;; code indentation
;; -----------------------------------------------------------------------------
;; http://emacsredux.com/blog/2013/03/27/indent-region-or-buffer/
;; https://www.emacswiki.org/emacs/ReformatBuffer
(defun indent-buffer ()
  "Indent the currently visited buffer."
  (interactive)
  (indent-region (point-min) (point-max)))

(defun indent-region-or-buffer ()
  "Indent a region if selected, otherwise the whole buffer."
  (interactive)
  (save-excursion
    (if (region-active-p)
        (progn
          (indent-region (region-beginning) (region-end))
          (message "Indented selected region."))
      (progn
        (indent-buffer)
        (message "Indented buffer.")))))
(global-set-key (kbd "<f12>") 'indent-region-or-buffer)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; speedbar
;; -----------------------------------------------------------------------------
;; https://emacs.stackexchange.com/questions/23786/sr-speedbar-shows-compilation-warning-at-startup
;; (require 'helm)
;; (require 'helm-config)
(setq byte-compile-warnings '(not free-vars ))

;; https://www.emacswiki.org/emacs/SrSpeedbar
(require 'sr-speedbar)

;; disable the icons
(setq speedbar-use-images nil)

;; set the default font size for just the sr-speedbar buffer to something other than my other buffers/windows
(make-face 'speedbar-face)
;; (set-face-font 'speedbar-face "Inconsolata-12")
(setq speedbar-mode-hook '(lambda () (buffer-face-set 'speedbar-face)))

(defadvice delete-other-windows (after my-sr-speedbar-delete-other-window-advice activate)
  "Check whether we are in speedbar, if it is, jump to next window."
  (let ()
    (when (and (sr-speedbar-window-exist-p sr-speedbar-window)
               (eq sr-speedbar-window (selected-window)))
      (other-window 1)
      )))
(ad-enable-advice 'delete-other-windows 'after 'my-sr-speedbar-delete-other-window-advice)
(ad-activate 'delete-other-windows)

;; With emacs 24.4+, ad-advised-definition-p function has been removed. You can add it back to fix the problem:
(defun ad-advised-definition-p (definition)
  "Return non-nil if DEFINITION was generated from advice information."
  (if (or (ad-lambda-p definition)
          (macrop definition)
          (ad-compiled-p definition))
      (let ((docstring (ad-docstring definition)))
        (and (stringp docstring)
             (get-text-property 0 'dynamic-docstring-function docstring)))))

;; Make Sr-speedbar open files in the next window, instead of in the previous window:
(defun select-next-window ()
  (other-window 1))

(defun my-sr-speedbar-open-hook ()
  (add-hook 'speedbar-before-visiting-file-hook 'select-next-window t)
  (add-hook 'speedbar-before-visiting-tag-hook 'select-next-window t)
  )

(advice-add 'sr-speedbar-open :after #'my-sr-speedbar-open-hook)

;; Sr-speedbar doesn't appear to control Speedbar: problem using Emacs on OSX (25.1)
;; fix so speedbar is in same window
(with-eval-after-load "speedbar"
  (autoload 'sr-speedbar-toggle "sr-speedbar" nil t)
  (global-set-key (kbd "s-s") 'sr-speedbar-toggle)
  )

;; load at start
;; https://superuser.com/questions/740118/loading-speedbar-on-emacs-startup
;; (add-hook 'emacs-startup-hook (lambda ()
;; (sr-speedbar-open)
;; ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; smooth scrolling
;; https://stackoverflow.com/questions/1128927/how-to-scroll-line-by-line-in-gnu-emacs
(require 'smooth-scrolling)
(smooth-scrolling-mode 1)
(setq smooth-scroll-margin 5)

;; rectangular region

;; C++
;; -----------------------------------------------------------------------------
;; https://github.com/DarthFennec/highlight-indent-guides
;; (add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
;; (setq highlight-indent-guides-method 'fill)

;; https://tuhdo.github.io/c-ide.html

;; 80 column rule
;; -----------------------------------------------------------------------------
;; https://www.emacswiki.org/emacs/FillColumnIndicator
;; https://www.emacswiki.org/emacs/fill-column-indicator.el
(require 'fill-column-indicator)

;; (add-hook 'sql-mode-hook 'fci-mode)
(setq-default fci-rule-column 80)

;; enabled fci-mode in text and programming modes, but not special buffers (dired, shell, ...)
(define-globalized-minor-mode global-fci-mode fci-mode
  (lambda ()
    (if (and
         (not (string-match "^\*.*\*$" (buffer-name)))
         (not (eq major-mode 'dired-mode)) ;; )
         (not (eq major-mode 'python-mode)))
        (fci-mode 1))))
(global-fci-mode 1)
(put 'upcase-region 'disabled nil)


;; highlight current line
;; -----------------------------------------------------------------------------
;; https://www.reddit.com/r/emacs/comments/74pz0h/how_to_draw_a_horizontal_line_on_current_line/
(global-hl-line-mode 1)
(set-face-background 'hl-line nil)
(set-face-foreground 'hl-line nil)
(set-face-underline  'hl-line t)

;; scala
;; -----------------------------------------------------------------------------
;; http://ensime.github.io/editors/emacs/install/
;; (use-package ensime
;;   :ensure t
;;   :pin melpa-stable)
;; (add-to-list 'exec-path "/usr/local/bin")
