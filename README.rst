============
Emacs config
============

macOS
-----
High Sierra 10.13.2 with `emacs <https://emacsformacosx.com/>` 25.3

Linux
-----
Ubuntu 16.04 LTS with emacs ??

Install
-------
.. code:: bash

    EMACS_DIR=${HOME}/.emacs.d
    
    if [ ! -d "$EMACS_DIR" ]; then
        mkdir ~/.emacs.d
    fi
    cd $EMACS_DIR ; git init
    git remote add origin https://gitlab.com/druprivate/emacs-config.git
    git pull origin master
    ./install.bash
