;; Generated package description from sql-indent.el
(define-package "sql-indent" "1.4" "Support for indenting code in SQL files." '((cl-lib "0.5")) :url "http://elpa.gnu.org/packages/sql-indent.html" :keywords '("languages" "sql"))
