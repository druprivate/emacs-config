#!/bin/bash -i

EMACS_DIR=${HOME}/.emacs.d

# https://askubuntu.com/questions/851633/emacs-25-on-ubuntu-16-10
sudo add-apt-repository -y ppa:kelleyk/emacs && \
sudo apt update && \
sudo apt install -y emacs25
# /usr/bin/emacs-25.325 --version
/usr/bin/emacs --version

echo -e $'
##########
# https://www.reddit.com/r/emacs/comments/86q5my/emacs_playing_nice_with_colors_in_a_tmux_console/
# EMACS="env TERM=xterm-256color /usr/bin/emacs-25.325 -q --load ~/.emacs.d/init.el -nw"
EMACS="env TERM=xterm-256color /usr/bin/emacs -q --load ~/.emacs.d/init.el -nw"
et()  { $EMACS "$@" ; }
etd() { $EMACS "$@" --debug-init ; }

# emacs client in terminal -t == -nw from man emacsclient)
ec() { env TERM=xterm-256color emacsclient -t "$@" ; }

# https://unix.stackexchange.com/questions/73484/how-can-i-set-vi-as-my-default-editor-in-unix
export VISUAL=$EMACS
export EDITOR="$VISUAL"

# start/stop the Emacs server
alias emstart="emacs --daemon -q --load ~/.emacs.d/init.el ; ps -ef | grep emacs | grep daemon"
alias emstop="emacsclient -e \'(kill-emacs)\'                ; ps -ef | grep emacs | grep daemon" # -e == eval
' >> ~/.bash_aliases

source ~/.bashrc

exit 0